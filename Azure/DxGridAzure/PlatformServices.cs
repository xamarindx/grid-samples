﻿using System;
using System.Collections.Generic;

namespace DxGridAzure {
    public interface IPlatformAzureService {
        TodoItemManager CreateTodoItemManager(string url, string key);
    }

    public static class PlatformServicesContainer {
        private static Dictionary<Type, object> services = new Dictionary<Type, object>();

        public static void Register<T>(T instance) {
            services[typeof(T)] = instance;
        }
        public static T Get<T>() {
            return (T)services[typeof(T)];
        }
    }
}


﻿using System;
using Newtonsoft.Json;

namespace DxGridAzure {
    public class TodoItem : ModelObject {
        string name;
        bool done;
        int order;

        public int Order { 
            get {
                return order;
            }
            set {
                if (order != value) {
                    order = value;
                    RaisePropertyChanged("Order");
                }
            }
        }

        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "text")]
        public string Name { 
            get { return name; }
            set { 
                if (name != value) {
                    name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }

        [JsonProperty(PropertyName = "complete")]
        public bool Done { 
            get { return done; }
            set { 
                if (done != value) {
                    done = value;
                    RaisePropertyChanged("Done");
                }
            }
        }
    }
}


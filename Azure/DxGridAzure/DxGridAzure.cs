﻿using System;

using Xamarin.Forms;

namespace DxGridAzure {
    public class App : Application {
        TodoItemManager todoItemManager;
        TodoListModel listModel;

        public App() {
            //todoItemManager = PlatformServicesContainer.Get<IPlatformAzureService>().CreateTodoItemManager("https://azsyd1.azure-mobile.net/", "fZmeuozTJwWwhcDdHahUrWboqIJAJu67");
            todoItemManager = PlatformServicesContainer.Get<IPlatformAzureService>().CreateTodoItemManager(@"https://1f03a995-0ee0-4-231-b9ee.azurewebsites.net/", "ZUMOAPPKEY");
            listModel = new TodoListModel(todoItemManager);
            MainPage = new NavigationPage(new TodoListPage(listModel));
        }

        protected override async void OnStart() {
            await listModel.RefreshAsync();
        }
        protected override async void OnResume() {
            await listModel.RefreshAsync();
        }
        protected override void OnSleep() {
            // Handle when your app sleeps
        }
    }
}


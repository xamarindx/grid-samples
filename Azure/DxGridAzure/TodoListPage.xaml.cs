﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using DevExpress.Mobile.DataGrid;

namespace DxGridAzure {
    public partial class TodoListPage : ContentPage {
        public TodoListPage(TodoListModel model) {
            InitializeComponent();
            BindingContext = model;
        }

        void OnSwipeButtonClick(object sender, SwipeButtonEventArgs e) {
            if (e.ButtonInfo.ButtonName == "RemoveButton")
                grid.DeleteRow(e.RowHandle);
        }
        void OnInitNewRow(object sender, InitNewRowEventArgs e) {
            TodoListModel model = (TodoListModel)BindingContext;
            e.EditableRowData.SetFieldValue("Order", model.Items.Count);
        }
    }
}


﻿using System;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections.Generic;

using Microsoft.WindowsAzure.MobileServices;

namespace DxGridAzure {
    public class TodoItemManager {
        IMobileServiceTable<TodoItem> todoTable;

        public TodoItemManager(IMobileServiceTable<TodoItem> todoTable) {
            this.todoTable = todoTable;
        }

        public async Task<TodoItem> GetTaskAsync(string id) {
            try {
                return await todoTable.LookupAsync(id);
            } 
            catch (MobileServiceInvalidOperationException msioe) {
                Debug.WriteLine(@"INVALID {0}", msioe.Message);
            }
            catch (Exception e) {
                Debug.WriteLine(@"ERROR {0}", e.Message);
            }
            return null;
        }
        public async Task<List<TodoItem>> GetTasksAsync() {
            try  {
                List<TodoItem> items = new List<TodoItem>(await todoTable.ReadAsync());

                int order = 0;
                foreach (TodoItem item in items)
                    item.Order = order++;

                return items;
            } catch (MobileServiceInvalidOperationException msioe) {
                Debug.WriteLine(@"INVALID {0}", msioe.Message);
            } catch (Exception e) {
                Debug.WriteLine(@"ERROR {0}", e.Message);
            }
            return null;
        }
        public async Task SaveTaskAsync(TodoItem item) {
            if (item.ID == null)
                await todoTable.InsertAsync(item);
            else
                await todoTable.UpdateAsync(item);
        }
        public async Task DeleteTaskAsync (TodoItem item) {
            try {
                await todoTable.DeleteAsync(item);
            } catch (MobileServiceInvalidOperationException msioe) {
                Debug.WriteLine(@"INVALID {0}", msioe.Message);
            } catch (Exception e) {
                Debug.WriteLine(@"ERROR {0}", e.Message);
            }
        }
    }
}


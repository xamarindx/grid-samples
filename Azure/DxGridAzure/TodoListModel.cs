﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace DxGridAzure {
    public class TodoListModel : ModelObject {
        readonly TodoItemManager manager;
        ObservableCollection<TodoItem> items;

        public ObservableCollection<TodoItem> Items {
            get { return items; }
            set { 
                if (items != value) {
                    if (items != null) {
                        items.CollectionChanged -= ItemCollectionChanged;
                        foreach (TodoItem item in items)
                            item.PropertyChanged -= ItemChanged;
                    }

                    items = value;

                    if (items != null) {
                        items.CollectionChanged += ItemCollectionChanged;
                        foreach (TodoItem item in items)
                            item.PropertyChanged += ItemChanged;
                    }

                    RaisePropertyChanged("Items");
                }
            }
        }

        public TodoListModel(TodoItemManager manager) {
            this.manager = manager;
        }

        async void ItemCollectionChanged(object sender, NotifyCollectionChangedEventArgs args) {
            switch (args.Action) {
                case NotifyCollectionChangedAction.Add:
                    foreach (TodoItem item in args.NewItems)
                        await manager.SaveTaskAsync(item);
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (TodoItem item in args.OldItems)
                        await manager.DeleteTaskAsync(item);
                    break;
                case NotifyCollectionChangedAction.Move:
                case NotifyCollectionChangedAction.Replace:
                case NotifyCollectionChangedAction.Reset:
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        async void ItemChanged(object sender, PropertyChangedEventArgs args) {
            TodoItem item = sender as TodoItem;
            if (item != null)
                await manager.SaveTaskAsync(item);
        }
        public async Task RefreshAsync() {
            List<TodoItem> items = await manager.GetTasksAsync();
            Items = new ObservableCollection<TodoItem>(items);
        }
        public async Task DeleteAsync(int index) {
            if ((index >= 0) && (index < Items.Count))
                await manager.DeleteTaskAsync(Items[index]);
        }
    }
}


﻿using System;
using Microsoft.WindowsAzure.MobileServices;

namespace DxGridAzure.iOS {
    public class PlatformAzureService: IPlatformAzureService {
        #region IPlatformAzureService implementation

        TodoItemManager IPlatformAzureService.CreateTodoItemManager(string url, string key) {
            MobileServiceClient client = new MobileServiceClient(url, key);
            IMobileServiceTable<TodoItem> todoTable = client.GetTable<TodoItem>(); 
            return new TodoItemManager(todoTable);
        }

        #endregion
    }
}


﻿using System;
using System.Globalization;
using DevExpress.Mobile.Core;
using Xamarin.Forms;

namespace DevExpress.GridDemo {
    public class VisibilityStateToBoolValueConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            VisibilityState state = (VisibilityState)value;
            return state != VisibilityState.Never;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            return (bool)value ? VisibilityState.Always : VisibilityState.Never;
        }
    }
}
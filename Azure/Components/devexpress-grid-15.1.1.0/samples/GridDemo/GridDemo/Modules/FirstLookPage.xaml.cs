﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace DevExpress.GridDemo {
    public partial class FirstLookPage {
        public FirstLookPage() {
            InitializeComponent();
            BindData();
            TargetIdiom idiom = Device.Idiom;
            idiom = TargetIdiom.Unsupported;
        }

        async void BindData() {
            MainPageViewModel model = await LoadData();
            BindingContext = model;
        }
        Task<MainPageViewModel> LoadData() {
            return Task<MainPageViewModel>.Run(() => new MainPageViewModel(new DemoOrdersRepository()));
        }
    }
}

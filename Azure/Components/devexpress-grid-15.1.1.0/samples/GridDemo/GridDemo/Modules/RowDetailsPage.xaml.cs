﻿using DevExpress.Mobile.DataGrid;
using System.Threading.Tasks;

namespace DevExpress.GridDemo {
    public partial class RowDetailsPage {
        public RowDetailsPage() {
            InitializeComponent();
            BindData();
        }
        void OnGridSelectionChanged(object sender, GridSelectionChangedEventArgs e) {
            UpdateDetails();
        }
        void UpdateDetails() {
            Customer customer = grid.SelectedDataObject as Customer;
            if(customer == null)
                return;
            
            photo.Source = customer.Photo;
            name.Text = customer.Name;
            position.Text = customer.Position;
            hiredDate.Text = customer.HireDate.ToString("d");
            phone.Text = customer.Phone;
            notes.Text = customer.Notes;
        }
        async void BindData() {
            BindingContext = await LoadData();
            UpdateDetails();
        }
        Task<MainPageViewModel> LoadData() {
            return Task<MainPageViewModel>.Run(() => new MainPageViewModel(new DemoOrdersRepository()));
        }
    }
}

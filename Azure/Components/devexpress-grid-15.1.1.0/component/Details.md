Your Next Great Mobile App Starts Here.  

Introduce high-impact, enterprise ready features to your next Android and iOS app with the DevExpress Grid for Xamarin.Forms. Available free-of-charge, the DevExpress Grid ships with high-performance capabilities designed to fully address the data visualization and touch-first requirements of todayís BYOD world.  

The DevExpress Grid is currently in beta. Features include the following:  

* Flexible data binding options
* Multiple column types (images, text, numbers, dates, etc.)
* Grid cell templates
* Auto generated columns for data source fields
* Calculated (unbound) columns
* Sorting against multiple columns
* Outlook® inspired Data grouping
* Excel® inspired Filtering (auto filter panel, multi-column filtering expressions)
* Automatic data summary computation (total and group summaries)
* Five predefined aggregate functions and ability to specify custom rules
* Pull-to-refresh functionality
* Load-more functionality
* Custom cell appearances
* Column resizing
* New item row
* In-place data editing
* Built-in popup menus
* Column chooser
* Swipe buttons
* Localization
* Theming